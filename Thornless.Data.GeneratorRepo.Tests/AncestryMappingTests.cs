using System;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using AutoFixture;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using NUnit.Framework.Legacy;
using Thornless.Data.GeneratorRepo.DataModels;

namespace Thornless.Data.GeneratorRepo.Tests
{
    [TestFixture]
    public class AncestryMappingTests
    {
        private readonly Fixture _fixture = new Fixture();

        [Test]
        public async Task ListAncestries_MapsCorrectly()
        {
            var expectedAncestries = CreateAncestries();
            var repo = CreateInMemoryRepo(expectedAncestries);
            var actualAncestries = await repo.ListAncestries();

            ClassicAssert.AreEqual(expectedAncestries.Length, actualAncestries.Length);

            for (var i = 0; i < expectedAncestries.Length; i++)
            {
                var expectedAncestry = expectedAncestries[0];
                var actualAncestry = actualAncestries.FirstOrDefault(x => x.Code == expectedAncestry.Code);
                ClassicAssert.NotNull(actualAncestry);
                ClassicAssert.AreEqual(expectedAncestry.Copyright, actualAncestry!.Copyright);
                ClassicAssert.AreEqual(expectedAncestry.FlavorHtml, actualAncestry.FlavorHtml);
                ClassicAssert.AreEqual(expectedAncestry.LastUpdatedDate, actualAncestry.LastUpdatedDate);
                ClassicAssert.AreEqual(expectedAncestry.Name, actualAncestry.Name);
                ClassicAssert.AreEqual(expectedAncestry.SortOrder, actualAncestry.SortOrder);
            }
        }

        [Test]
        public async Task GetAncestry_MapsCorrectly()
        {
            var expectedAncestries = CreateAncestries();
            var repo = CreateInMemoryRepo(expectedAncestries);

            var expectedAncestry = expectedAncestries[1];
            var actualAncestry = await repo.GetAncestry(expectedAncestry.Code);

            ClassicAssert.NotNull(actualAncestry);
            ClassicAssert.AreEqual(expectedAncestry.Copyright, actualAncestry!.Copyright);
            ClassicAssert.AreEqual(expectedAncestry.FlavorHtml, actualAncestry!.FlavorHtml);
            ClassicAssert.AreEqual(expectedAncestry.LastUpdatedDate, actualAncestry!.LastUpdatedDate);
            ClassicAssert.AreEqual(expectedAncestry.Name, actualAncestry!.Name);
            ClassicAssert.AreEqual(expectedAncestry.SortOrder, actualAncestry!.SortOrder);

            ClassicAssert.AreEqual(expectedAncestry.NameParts.Count(), actualAncestry!.NameParts.Count());
            for (var i = 0; i < expectedAncestry.NameParts.Count(); i++)
            {
                var expectedNamePart = expectedAncestry.NameParts[i]!;
                var actualNamePart = actualAncestry!.NameParts[i]!;

                ClassicAssert.AreEqual(ParseJsonArray(expectedNamePart.NameMeaningsJson), actualNamePart.NameMeanings);
                ClassicAssert.AreEqual(ParseJsonArray(expectedNamePart.NamePartsJson), actualNamePart.NameParts);
                ClassicAssert.AreEqual(expectedNamePart.NameSegmentCode, actualNamePart.NameSegmentCode);
                ClassicAssert.AreEqual(expectedNamePart.RandomizationWeight, actualNamePart.RandomizationWeight);
            }

            ClassicAssert.AreEqual(expectedAncestry.Options.Count(), actualAncestry.Options.Count());
            for (var i = 0; i < expectedAncestry.Options.Count(); i++)
            {
                var expectedOption = expectedAncestry.Options[i];
                var actualOption = actualAncestry.Options[i];

                ClassicAssert.AreEqual(expectedOption.Code, actualOption.Code);
                ClassicAssert.AreEqual(expectedOption.Name, actualOption.Name);
                ClassicAssert.AreEqual(ParseJsonArray(expectedOption.NamePartSeperatorJson), actualOption.NamePartSeperators);
                
                ClassicAssert.AreEqual(expectedOption.SegmentGroups.Count(), actualOption.SegmentGroups.Count());
                for (var j = 0; j < expectedOption.SegmentGroups.Count(); j++)
                {
                    var expectedSegmentGroup = expectedOption.SegmentGroups[j];
                    var actualSegmentGroup = actualOption.SegmentGroups[j];

                    ClassicAssert.AreEqual(ParseJsonArray(expectedSegmentGroup.NameSegmentCodesJson), actualSegmentGroup.NameSegmentCodes);
                    ClassicAssert.AreEqual(expectedSegmentGroup.RandomizationWeight, actualSegmentGroup.RandomizationWeight);
                }

                ClassicAssert.AreEqual(expectedOption.SeperatorChancePercentage, actualOption.SeperatorChancePercentage);
                ClassicAssert.AreEqual(expectedOption.SortOrder, actualOption.SortOrder);
            }
        }

        private static string[] ParseJsonArray(string s)
        {
            return JsonSerializer.Deserialize<string[]>(s) ?? Array.Empty<string>();
        }

        private static CharacterNameRepository CreateInMemoryRepo(CharacterAncestryDto[] ancestries)
        {
            var options = new DbContextOptionsBuilder<GeneratorContext>()
                    .UseSqlite("DataSource=:memory:")
                    .Options;

            var context = new GeneratorContext(options);
            context.Database.OpenConnection();
            context.Database.EnsureCreated();
            context.AddRange(ancestries);
            context.SaveChanges();

            return new CharacterNameRepository(context);
        }

        private CharacterAncestryDto[] CreateAncestries()
        {
            var ancestries = _fixture.Build<CharacterAncestryDto>()
                                    .With(x => x.NameParts,
                                            _fixture.Build<CharacterAncestryNamePartDto>()
                                                    .With(x => x.NameMeaningsJson, CreateJsonArrayString())
                                                    .With(x => x.NamePartsJson, CreateJsonArrayString())
                                                    .CreateMany(3)
                                                    .ToList())
                                    .With(x => x.Options,
                                            _fixture.Build<CharacterAncestryOptionDto>()
                                                    .With(x => x.NamePartSeperatorJson, CreateJsonArrayString())
                                                    .With(x => x.SegmentGroups, _fixture.Build<CharacterAncestrySegmentGroupDto>()
                                                                                        .With(x => x.NameSegmentCodesJson, CreateJsonArrayString())
                                                                                        .CreateMany(3)
                                                                                        .ToList())
                                                    .CreateMany(3)
                                                    .ToList())
                                    .CreateMany(3)
                                    .ToArray();
            return ancestries;
        }

        private string CreateJsonArrayString()
        {
            var strings = _fixture.CreateMany<string>();
            return JsonSerializer.Serialize(strings);
        }
    }
}
