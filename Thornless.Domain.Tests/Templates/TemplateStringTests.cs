using System.Linq;
using AutoFixture;
using NUnit.Framework;
using NUnit.Framework.Legacy;
using Thornless.Domain.Templates;

namespace Thornless.Domain.Tests.Templates
{
    public class TemplateStringTests
    {
        private readonly Fixture _fixture = new Fixture();

        [Test]
        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        public void GivenStringItem_WhenNFields_ParsesCorrectly(int num)
        {
            var strings = _fixture.CreateMany<string>(num).ToArray();

            var templatedStrings = strings.Select(x => "{" + x + "}").ToArray();

            var stringFormat = "start " + string.Join(" sep ", templatedStrings) + "end";

            var templateString = new TemplateString(stringFormat);

            ClassicAssert.AreEqual(stringFormat, templateString.StringFormat);
            ClassicAssert.AreEqual(num, templateString.TemplateFields.Length);
            for (int i = 0; i < num; i++)
            {
                ClassicAssert.AreEqual(strings[i], templateString.TemplateFields[i].FieldName);
                ClassicAssert.AreEqual(templatedStrings[i], templateString.TemplateFields[i].FieldTemplate);
            }
        }
    }
}
